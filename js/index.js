//jQuery to collapse the navbar on scroll
// $(window).scroll(function () {
// 	if ($('.navbar').offset().top > 50) {
// 		$('.navbar-fixed-top').addClass('top-nav-collapse');
// 	} else {
// 		$('.navbar-fixed-top').removeClass('top-nav-collapse');
// 	}
// });

$(function () {
	$('.navbar-brand.active').bind(
		'click',
		function (event) {
			$(
				'.page-scroll .navbar-link.active'
			).removeClass('active');
		}
	);
});

//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function () {
	$('.page-scroll .navbar-link').bind(
		'click',
		function (event) {
			var $anchor = $(this);
			var $navbarLinks = $(
				'.page-scroll .navbar-link'
			);
			$('html, body')
				.stop()
				.animate(
					{
						scrollTop: $(
							$anchor.attr('href')
						).offset().top,
					},
					100,
					'swing'
				);
			event.preventDefault();
			$navbarLinks.removeClass(
				'active'
			);
			$anchor.addClass('active');
		}
	);
});
